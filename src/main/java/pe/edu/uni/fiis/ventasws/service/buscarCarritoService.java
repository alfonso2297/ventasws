package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoResponse;

public interface buscarCarritoService {

    buscarCarritoResponse buscarCarrito(buscarCarritoRequest request);


}
