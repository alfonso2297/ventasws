package pe.edu.uni.fiis.ventasws.domain.rest;

import pe.edu.uni.fiis.ventasws.domain.Cliente;

import java.util.List;

public class regClienteResponse {

    private List<Cliente> lista;

    public List<Cliente> getLista() {
        return lista;
    }

    public void setLista(List<Cliente> lista) {
        this.lista = lista;
    }
}
