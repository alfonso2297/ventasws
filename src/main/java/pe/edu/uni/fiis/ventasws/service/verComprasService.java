package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;

public interface verComprasService {

    verComprasResponse buscarCompras(verComprasRequest request);

}
