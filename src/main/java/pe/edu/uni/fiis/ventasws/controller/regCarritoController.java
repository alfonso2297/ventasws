package pe.edu.uni.fiis.ventasws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoResponse;
import pe.edu.uni.fiis.ventasws.service.regCarritoService;

@RequestMapping(value = "carrito")
@Controller
public class regCarritoController {

        @Autowired
        private regCarritoService service;
        @RequestMapping (value = "add",
                method = RequestMethod.POST,
                produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
        public @ResponseBody
        regCarritoResponse registrarCarrito(
                @RequestBody regCarritoRequest request){
            return service.registrarCarrito(request);
        }


    }
