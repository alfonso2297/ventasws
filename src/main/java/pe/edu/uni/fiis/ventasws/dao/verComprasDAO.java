package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;

public interface verComprasDAO {

    verComprasResponse buscarCompras(verComprasRequest request);

}
