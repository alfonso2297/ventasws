package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoResponse;

public interface regCarritoService {

    regCarritoResponse registrarCarrito(regCarritoRequest request);

}
