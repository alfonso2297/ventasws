package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.regComprasDAO;
import pe.edu.uni.fiis.ventasws.domain.Compras;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasResponse;

import java.util.ArrayList;


@Repository
public class regComprasDaoImpl extends BaseDAO implements regComprasDAO {

    private String agregarCompraSQL= " Insert into COMPRAS(ID_COMPRA,ID_PROVEEDOR, C_POLLO,C_PAVITA, C_HUEVOS,IGV) " +
            " VALUES (?,?,?,?,?,?) ";

    public regComprasResponse registrarCompra(regComprasRequest request) {
        regComprasResponse response= new regComprasResponse();
        response.setLista(new ArrayList<Compras>());
        Compras i = new Compras();
        i.setId_compras(request.getId_compras());
        i.setId_proveedor(request.getId_proveedor());
        i.setC_pollo(request.getC_pollo());
        i.setC_pavita(request.getC_pavita());
        i.setC_huevos(request.getPrecio());
        i.setIgv(request.getIgv());
        response.getLista().add(i);

        getJdbcTemplate().update(agregarCompraSQL,
                new Object[]{
                        request.getId_compras(),
                        request.getId_proveedor(),
                        request.getC_pollo(),
                        request.getC_pavita(),
                        request.getC_huevos(),
                        request.getPrecio(),
                        request.getIgv()
                }
        );
        return response;
    }
}
