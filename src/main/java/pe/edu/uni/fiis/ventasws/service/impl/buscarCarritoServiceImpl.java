package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.buscarCarritoDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoResponse;
import pe.edu.uni.fiis.ventasws.service.buscarCarritoService;


@Service
@Transactional
public class buscarCarritoServiceImpl implements buscarCarritoService {

    @Autowired
    private buscarCarritoDAO dao;


    public buscarCarritoResponse buscarCarrito(buscarCarritoRequest request) {

        return dao.buscarCarrito(request);
    }


}
