package pe.edu.uni.fiis.ventasws.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;
import pe.edu.uni.fiis.ventasws.service.regArticuloService;

@RequestMapping(value = "articulos")
@Controller
public class RegArticulosController {
    @Autowired
    private regArticuloService service;
    @RequestMapping(value = "regArticulo",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody regArticulosResponse regArticulos (
            @RequestBody regArticulosRequest request){
            return service.registrarArticulo(request);
    }


}

