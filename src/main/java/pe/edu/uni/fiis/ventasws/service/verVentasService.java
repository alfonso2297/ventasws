package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.verVentasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasResponse;

public interface verVentasService {

    verVentasResponse buscarVentas(verVentasRequest request);

}
