package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.regArticuloDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;
import pe.edu.uni.fiis.ventasws.service.regArticuloService;

@Service
@Transactional
public class regArticuloServiceImpl implements regArticuloService {
    @Autowired
    public regArticuloDAO dao;

    public regArticulosResponse registrarArticulo(regArticulosRequest request) {

        return dao.registrarArticulo(request);
    }

}
