package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.regComprasDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;
import pe.edu.uni.fiis.ventasws.service.regComprasService;
@Service
@Transactional
public class regComprasServiceImpl implements regComprasService {
    @Autowired
    private regComprasDAO dao;

    public regComprasResponse registrarCompra(regComprasRequest request) {
        return dao.registrarCompra(request);
    }
}
