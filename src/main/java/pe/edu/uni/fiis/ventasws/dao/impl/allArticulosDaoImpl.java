package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.allArticulosDAO;
import pe.edu.uni.fiis.ventasws.domain.Articulo;
import pe.edu.uni.fiis.ventasws.domain.rest.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class allArticulosDaoImpl extends BaseDAO implements allArticulosDAO {

    private String mostrarArticulosSQL=
            " SELECT ID_ARTICULO, ID_CATEGORIA, NOMBRE_ARTICULO, DESCRIPCION_ARTICULO, PRECIO, CANTIDAD_STOCK, IMAGEN  FROM ARTICULO  WHERE ID_ARTICULO= ?  OR 1=1 ";

    public allArticulosResponse mostrarArticulos(allArticulosRequest request) {
        allArticulosResponse response = new allArticulosResponse();
        response.setLista(new ArrayList<Articulo>());

        List<Map<String,Object>> listaSQL = getJdbcTemplate().queryForList(mostrarArticulosSQL
                ,new Object[]{request.getId_articulo() }
                );

        for (Map<String,Object> item : listaSQL
        ) {
            Articulo articulo = new Articulo();
            articulo.setId_articulo(((BigDecimal) item.get("ID_ARTICULO")).intValue());
            articulo.setId_categoria(((BigDecimal) item.get("ID_CATEGORIA")).intValue());
            articulo.setNombre_articulo((String) item.get("NOMBRE_ARTICULO"));
            articulo.setDescripcion_articulo((String) item.get("DESCRIPCION_ARTICULO"));
            articulo.setPrecio(((BigDecimal) item.get("PRECIO")).doubleValue());
            articulo.setCantidad_stock(((BigDecimal) item.get("CANTIDAD_STOCK")).intValue());
            articulo.setImagen((String) item.get("IMAGEN"));
            response.getLista().add(articulo);
        }
        return response;
    }


}
