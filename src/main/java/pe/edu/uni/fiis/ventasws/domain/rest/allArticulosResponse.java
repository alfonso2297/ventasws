package pe.edu.uni.fiis.ventasws.domain.rest;

import pe.edu.uni.fiis.ventasws.domain.Articulo;

import java.util.List;

public class allArticulosResponse {

    private List<Articulo> lista;

    public List<Articulo> getLista() {
        return lista;
    }

    public void setLista(List<Articulo> lista) {
        this.lista = lista;
    }

}
