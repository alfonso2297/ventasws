package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;

public interface regArticuloDAO {
    regArticulosResponse registrarArticulo(regArticulosRequest request);
}
