package pe.edu.uni.fiis.ventasws.domain.rest;

import pe.edu.uni.fiis.ventasws.domain.Carrito;

import java.util.List;

public class regCarritoResponse {

    private List<Carrito> lista;

    public List<Carrito> getLista() {
        return lista;
    }

    public void setLista(List<Carrito> lista) {
        this.lista = lista;
    }
}
