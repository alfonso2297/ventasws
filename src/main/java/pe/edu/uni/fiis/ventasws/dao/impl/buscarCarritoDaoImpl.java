package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.buscarCarritoDAO;
import pe.edu.uni.fiis.ventasws.domain.Articulo;
import pe.edu.uni.fiis.ventasws.domain.Carrito;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
public class buscarCarritoDaoImpl extends BaseDAO implements buscarCarritoDAO {


    private String mostrarCarritoSQL=
            " SELECT * FROM CARRITO  WHERE ID_USUARIO= ? ";

    public buscarCarritoResponse buscarCarrito(buscarCarritoRequest request) {
        buscarCarritoResponse response = new buscarCarritoResponse();
        response.setLista(new ArrayList<Carrito>());

        List<Map<String,Object>> listaSQL = getJdbcTemplate().queryForList(mostrarCarritoSQL
                ,new Object[]{request.getId_usuario() }
        );

        for (Map<String,Object> item : listaSQL
        ) {
            Carrito carrito= new Carrito();
            carrito.setId_carrito(((BigDecimal) item.get("ID_CARRITO")).intValue());
            carrito.setId_usuario(((BigDecimal) item.get("ID_USUARIO")).intValue());
            carrito.setId_producto(((BigDecimal) item.get("ID_PRODUCTO")).intValue());
            carrito.setFecha((String) item.get("FECHA"));
            carrito.setId_estado(((BigDecimal) item.get("ID_ESTADO")).intValue());
            carrito.setSubtotal(((BigDecimal) item.get("SUBTOTAL")).doubleValue());
            response.getLista().add(carrito);
        }
        return response;
    }





}
