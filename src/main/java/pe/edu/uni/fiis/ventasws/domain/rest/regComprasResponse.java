package pe.edu.uni.fiis.ventasws.domain.rest;

import pe.edu.uni.fiis.ventasws.domain.Compras;

import java.util.List;

public class regComprasResponse {

    private List<Compras> lista;

    public List<Compras> getLista() {
        return lista;
    }

    public void setLista(List<Compras> lista) {
        this.lista = lista;
    }
}


