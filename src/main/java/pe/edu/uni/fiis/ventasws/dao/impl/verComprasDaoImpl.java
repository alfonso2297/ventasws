package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.verComprasDAO;
import pe.edu.uni.fiis.ventasws.domain.Compras;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class verComprasDaoImpl extends BaseDAO implements verComprasDAO {

    private String buscarComprasSQL=
            " SELECT ID_COMPRA,ID_PROVEEDOR,C_POLLO,C_PAVITA,C_HUEVOS,PRECIO,IGV " +
                    " FROM COMPRAS " +
                    " WHERE ID_PROVEEDOR=? ";

    public verComprasResponse buscarCompras(verComprasRequest request) {
        verComprasResponse response = new verComprasResponse();
        response.setLista(new ArrayList<Compras>());

        List<Map<String,Object>> listaSQL = getJdbcTemplate().queryForList(buscarComprasSQL);

        for (Map<String,Object> item : listaSQL
                ) {
            Compras compras = new Compras();
            compras.setId_compras(((BigDecimal)item.get("ID_COMPRA")).intValue());
            compras.setId_proveedor(((BigDecimal)item.get("ID_PROVEEDOR")).intValue());
            compras.setC_pollo(((BigDecimal)item.get("C_POLLO")).intValue());
            compras.setC_pavita(((BigDecimal)item.get("C_PAVITA")).intValue());
            compras.setC_huevos(((BigDecimal)item.get("C_HUEVOS")).intValue());
            compras.setPrecio(((BigDecimal)item.get("PRECIO")).doubleValue());
            compras.setIgv(((BigDecimal)item.get("IGV")).intValue());
            response.getLista().add(compras);
        }
        return response;
    }
}
