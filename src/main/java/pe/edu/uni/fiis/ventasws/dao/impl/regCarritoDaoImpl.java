package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.regCarritoDAO;
import pe.edu.uni.fiis.ventasws.domain.Carrito;
import pe.edu.uni.fiis.ventasws.domain.Cliente;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;

import java.util.ArrayList;

@Repository
public class regCarritoDaoImpl extends BaseDAO implements regCarritoDAO {

    private String agregarCarritoSQL= " INSERT INTO CARRITO(ID_CARRITO, ID_USUARIO, ID_PRODUCTO, FECHA, ID_ESTADO, SUBTOTAL) VALUES " +
            "(?,?,?,?,?,?)";

    public regCarritoResponse registrarCarrito(regCarritoRequest request) {
        regCarritoResponse response = new regCarritoResponse();
        response.setLista(new ArrayList<Carrito>());
        Carrito i = new Carrito();
        i.setId_carrito(request.getId_carrito());
        i.setId_usuario(request.getId_usuario());
        i.setId_producto(request.getId_producto());
        i.setFecha(request.getFecha());
        i.setId_estado(request.getId_estado());
        i.setSubtotal(request.getSubtotal());
        response.getLista().add(i);

        getJdbcTemplate().update(agregarCarritoSQL,
                request.getId_carrito(),
                request.getId_usuario(),
                request.getId_producto(),
                request.getFecha(),
                request.getId_estado(),
                request.getSubtotal()
        );

        return response;
    }
}
