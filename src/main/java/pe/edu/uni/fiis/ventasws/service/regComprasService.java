package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.regComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasResponse;

public interface regComprasService {

    regComprasResponse registrarCompra (regComprasRequest request);

}
