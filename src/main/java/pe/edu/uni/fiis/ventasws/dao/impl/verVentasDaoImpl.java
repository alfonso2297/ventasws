package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.verVentasDAO;
import pe.edu.uni.fiis.ventasws.domain.Usuario;
import pe.edu.uni.fiis.ventasws.domain.Ventas;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class verVentasDaoImpl extends BaseDAO implements verVentasDAO{

    private String buscarVentasSQL=
            " SELECT * " +
                    " FROM ARTICULO " +
                    " WHERE ID_ARTICULO=? OR 1=1 ";

    public verVentasResponse buscarVentas(verVentasRequest request) {
        verVentasResponse response = new verVentasResponse();
        response.setLista(new ArrayList<Ventas>());

        List<Map<String,Object>> listaSQL = getJdbcTemplate().queryForList(buscarVentasSQL,new Object[]{request.getId_articulo()});

        for (Map<String,Object> item : listaSQL
                ) {
            Ventas ventas = new Ventas();
            ventas.setId_articulo(((BigDecimal)item.get("ID_ARTICULO")).intValue());
            ventas.setId_categoria(((BigDecimal)item.get("ID_CATEGORIA")).intValue());
            ventas.setNombre_articulo((String)item.get("NOMBRE_ARTICULO"));
            ventas.setDescripcion_articulo((String)item.get("DESCRIPCION_ARTICULO"));
            ventas.setPrecio(((BigDecimal)item.get("PRECIO")).doubleValue());
            ventas.setCantidad_stock(((BigDecimal)item.get("CANTIDAD_STOCK")).intValue());
            ventas.setImagen((String)item.get("IMAGEN"));
            response.getLista().add(ventas);
        }
        return response;
    }
}
