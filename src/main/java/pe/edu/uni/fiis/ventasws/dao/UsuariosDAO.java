package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosResponse;

public interface UsuariosDAO {

    UsuariosResponse buscarUsuarios(UsuariosRequest request);

}
