package pe.edu.uni.fiis.ventasws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;
import pe.edu.uni.fiis.ventasws.service.buscarCarritoService;
import pe.edu.uni.fiis.ventasws.service.regArticuloService;


@RequestMapping(value = "carrito")
@Controller
public class buscarCarritoController {

        @Autowired
        private buscarCarritoService service;
        @RequestMapping(value = "verCarrito",
                method = RequestMethod.POST,
                produces = MediaType.APPLICATION_JSON_UTF8_VALUE
        )
        public @ResponseBody
        buscarCarritoResponse buscarCarrito(
                @RequestBody buscarCarritoRequest request){
            return service.buscarCarrito(request);
        }








    }
