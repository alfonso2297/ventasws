package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;

public interface regArticuloService {
    regArticulosResponse registrarArticulo(regArticulosRequest request);
}
