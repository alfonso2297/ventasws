package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.regArticuloDAO;
import pe.edu.uni.fiis.ventasws.domain.Articulo;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;

import java.util.ArrayList;


@Repository
public class regArticulosDaoImpl extends BaseDAO implements regArticuloDAO  {
    private String agregarClienteSQL= " INSERT INTO ARTICULO(ID_ARTICULO, ID_CATEGORIA, NOMBRE_ARTICULO, DESCRIPCION_ARTICULO, PRECIO, CANTIDAD_STOCK,IMAGEN)" +
            " VALUES (?,?,?,?,?,?,?) ";

    public regArticulosResponse registrarArticulo(regArticulosRequest request) {
        regArticulosResponse response = new regArticulosResponse();
        response.setLista(new ArrayList<Articulo>());
        Articulo i = new Articulo();
        i.setId_articulo(request.getId_articulo());
        i.setId_categoria(request.getId_categoria());
        i.setNombre_articulo(request.getNombre_articulo());
        i.setDescripcion_articulo(request.getDescripcion_articulo());
        i.setPrecio(request.getPrecio());
        i.setCantidad_stock(request.getCantidad_stock());
        i.setImagen(request.getImagen());
        response.getLista().add(i);

        getJdbcTemplate().update(agregarClienteSQL,
                request.getId_articulo(),
                request.getId_categoria(),
                request.getNombre_articulo(),
                request.getDescripcion_articulo(),
                request.getPrecio(),
                request.getCantidad_stock(),
                request.getImagen()
        );

        return response;
    }


}
