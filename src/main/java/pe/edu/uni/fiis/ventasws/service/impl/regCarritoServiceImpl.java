package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.regCarritoDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;
import pe.edu.uni.fiis.ventasws.service.regCarritoService;

@Service
@Transactional
public class regCarritoServiceImpl implements regCarritoService {

    @Autowired
    private regCarritoDAO dao;

    public regCarritoResponse registrarCarrito(regCarritoRequest request) {

        return dao.registrarCarrito(request);
    }



}
