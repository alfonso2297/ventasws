package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosResponse;

public interface UsuariosService {

    UsuariosResponse buscarUsuarios(UsuariosRequest request);

}
