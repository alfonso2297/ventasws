package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.buscarCarritoResponse;

public interface buscarCarritoDAO {

    buscarCarritoResponse buscarCarrito(buscarCarritoRequest request);

}
