package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;

public interface regClienteDAO {
    regClienteResponse registrarCliente (regClienteRequest request);
}
