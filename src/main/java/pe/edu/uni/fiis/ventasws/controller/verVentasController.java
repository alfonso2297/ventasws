package pe.edu.uni.fiis.ventasws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasResponse;
import pe.edu.uni.fiis.ventasws.service.verVentasService;

@RequestMapping(value = "ventas")
@Controller
public class verVentasController {
    @Autowired
    private verVentasService service;
    @RequestMapping(value = "buscarVentas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )

    public @ResponseBody verVentasResponse buscarVentas(
            @RequestBody verVentasRequest request){
        return service.buscarVentas(request);
    }
}
