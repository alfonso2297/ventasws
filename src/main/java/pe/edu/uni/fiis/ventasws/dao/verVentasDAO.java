package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.verVentasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasResponse;

public interface verVentasDAO {

    verVentasResponse buscarVentas(verVentasRequest request);

}
