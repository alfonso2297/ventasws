package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.verComprasDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;
import pe.edu.uni.fiis.ventasws.service.verComprasService;

@Service
@Transactional
public class verComprasServiceImpl implements verComprasService {

    @Autowired
    private verComprasDAO dao;

    public verComprasResponse buscarCompras(verComprasRequest request) {
        return dao.buscarCompras(request);
    }
}
