package pe.edu.uni.fiis.ventasws.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;
import pe.edu.uni.fiis.ventasws.service.regClienteService;

@RequestMapping(value = "sesion")
@Controller
public class regClienteController {
    @Autowired
    private regClienteService service;
    @RequestMapping(value = "regUsuario",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody regClienteResponse regCliente(
            @RequestBody regClienteRequest request
            ){
        return service.registrarCliente(request);
    }



}
