package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.UsuariosDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosResponse;
import pe.edu.uni.fiis.ventasws.service.UsuariosService;

@Service
@Transactional
public class UsuariosServiceImpl implements UsuariosService {
    @Autowired
    private UsuariosDAO dao;

    public UsuariosResponse buscarUsuarios(UsuariosRequest request) {
        return dao.buscarUsuarios(request);
    }
}
