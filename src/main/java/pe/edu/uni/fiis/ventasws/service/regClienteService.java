package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;

public interface regClienteService {
    regClienteResponse registrarCliente(regClienteRequest request);
}
