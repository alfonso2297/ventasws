package pe.edu.uni.fiis.ventasws.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verComprasResponse;
import pe.edu.uni.fiis.ventasws.service.verComprasService;

@Controller
@RequestMapping(value = "compras")
public class verComprasController {
    @Autowired
    private verComprasService service;
    @RequestMapping (value = "buscarCompras",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody verComprasResponse buscarCompras(
            @RequestBody verComprasRequest request){
        return service.buscarCompras(request);
    }




}
