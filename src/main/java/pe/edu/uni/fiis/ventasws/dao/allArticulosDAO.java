package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosResponse;

public interface allArticulosDAO {

    allArticulosResponse mostrarArticulos(allArticulosRequest request);

}
