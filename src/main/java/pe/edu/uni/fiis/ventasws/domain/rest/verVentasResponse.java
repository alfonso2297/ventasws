package pe.edu.uni.fiis.ventasws.domain.rest;

import pe.edu.uni.fiis.ventasws.domain.Ventas;

import java.util.List;

public class verVentasResponse {

    private List<Ventas> lista;

    public List<Ventas> getLista() {
        return lista;
    }

    public void setLista(List<Ventas> lista) {
        this.lista = lista;
    }
}
