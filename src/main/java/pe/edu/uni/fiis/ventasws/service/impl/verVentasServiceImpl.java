package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.verVentasDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.verVentasResponse;
import pe.edu.uni.fiis.ventasws.service.verVentasService;

@Service
@Transactional
public class verVentasServiceImpl implements verVentasService {
    @Autowired
    private verVentasDAO dao;

    public verVentasResponse buscarVentas(verVentasRequest request) {
        return dao.buscarVentas(request);
    }
}
