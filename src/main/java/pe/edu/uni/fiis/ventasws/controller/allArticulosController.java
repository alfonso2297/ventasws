package pe.edu.uni.fiis.ventasws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.ventasws.domain.rest.*;
import pe.edu.uni.fiis.ventasws.service.allArticulosService;
import pe.edu.uni.fiis.ventasws.service.verComprasService;
@RequestMapping(value = "articulos")
@Controller
public class allArticulosController {
    @Autowired
    private allArticulosService service;
    @RequestMapping (value = "allArticulos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody allArticulosResponse mostrarArticulos(
            @RequestBody allArticulosRequest request){
        return service.mostrarArticulos(request);
    }




}
