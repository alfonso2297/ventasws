package pe.edu.uni.fiis.ventasws.domain.rest;

public class UsuariosRequest {

    public String  nick;
    public String clave;

    public String getNick() {
        return nick;
    }

    public void setNick(String  nick) {
        this.nick = nick;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
