package pe.edu.uni.fiis.ventasws.service;

import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;

public interface allArticulosService {
    allArticulosResponse mostrarArticulos(allArticulosRequest request);
}
