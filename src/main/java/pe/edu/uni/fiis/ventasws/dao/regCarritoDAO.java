package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regCarritoResponse;

public interface regCarritoDAO {

    regCarritoResponse registrarCarrito(regCarritoRequest request);

}
