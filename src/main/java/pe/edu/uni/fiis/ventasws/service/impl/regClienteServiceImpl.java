package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.regClienteDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;
import pe.edu.uni.fiis.ventasws.service.regClienteService;

@Service
@Transactional
public class regClienteServiceImpl implements regClienteService {
    @Autowired
    private regClienteDAO dao;

    public regClienteResponse registrarCliente(regClienteRequest request) {

        return dao.registrarCliente(request);
    }
}
