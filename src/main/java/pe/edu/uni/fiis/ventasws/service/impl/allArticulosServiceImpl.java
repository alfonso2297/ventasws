package pe.edu.uni.fiis.ventasws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.ventasws.dao.allArticulosDAO;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.allArticulosResponse;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regArticulosResponse;
import pe.edu.uni.fiis.ventasws.service.allArticulosService;



@Service
@Transactional
public class allArticulosServiceImpl implements allArticulosService {
    @Autowired
    private allArticulosDAO dao;

    public allArticulosResponse mostrarArticulos(allArticulosRequest request) {

        return dao.mostrarArticulos(request);
    }

}
