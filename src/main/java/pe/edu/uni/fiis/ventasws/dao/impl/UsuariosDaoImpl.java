package pe.edu.uni.fiis.ventasws.dao.impl;


import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.UsuariosDAO;
import pe.edu.uni.fiis.ventasws.domain.Usuario;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.UsuariosResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class UsuariosDaoImpl extends BaseDAO implements UsuariosDAO {
    private String buscarUsuariosSQL =
            " SELECT nro_documento,USUARIO, clave,nombre, ape_paterno " +
                    " FROM COL_CLIENTE " +
                    " WHERE usuario= ? AND clave= ?";


    public UsuariosResponse buscarUsuarios(UsuariosRequest request) {
        UsuariosResponse response = new UsuariosResponse();
        response.setLista(new ArrayList<Usuario>());

        List<Map<String,Object>> listaSQL = getJdbcTemplate().queryForList(buscarUsuariosSQL,new Object[]{request.getNick(),request.getClave()});

        for (Map<String,Object> item : listaSQL
             ) {
            Usuario usuario = new Usuario();
            usuario.setId_Usuario(((BigDecimal)item.get("nro_documento")).intValue());
            usuario.setNick((String)item.get("usuario"));
            usuario.setClave((String)item.get("clave"));
            usuario.setNombres((String)item.get("nombre"));
            usuario.setApellidos((String)item.get("ape_paterno"));
            response.getLista().add(usuario);

        }
        return response;
    }
}
