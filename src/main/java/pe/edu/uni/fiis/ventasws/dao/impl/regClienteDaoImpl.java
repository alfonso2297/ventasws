package pe.edu.uni.fiis.ventasws.dao.impl;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ventasws.dao.BaseDAO;
import pe.edu.uni.fiis.ventasws.dao.regClienteDAO;
import pe.edu.uni.fiis.ventasws.domain.Cliente;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regClienteResponse;

import java.util.ArrayList;


@Repository
public class regClienteDaoImpl extends BaseDAO implements regClienteDAO {

    private String agregarClienteSQL= " INSERT INTO COL_CLIENTE(NRO_DOCUMENTO,USUARIO,CLAVE,NOMBRE,APE_PATERNO,APE_MATERNO,FECHA_REGISTRO,TIPO_DOC,DIRECCION,EMAIL,CELULAR,FECHA_NACIMIENTO,CODIGOPOSTAL,DEPARTAMENTO,PROVINCIA,DISTRITO) " +
            " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

    public regClienteResponse registrarCliente(regClienteRequest request) {
        regClienteResponse response = new regClienteResponse();
        response.setLista(new ArrayList<Cliente>());
        Cliente i = new Cliente();
        i.setNro_documento(request.getNro_documento());
        i.setUsuario(request.getUsuario());
        i.setClave(request.getClave());
        i.setNombre(request.getNombre());
        i.setApe_paterno(request.getApe_paterno());
        i.setApe_materno(request.getApe_materno());
        i.setFecha_registro(request.getFecha_registro());
        i.setTipo_doc(request.getTipo_doc());
        i.setDireccion(request.getDireccion());
        i.setEmail(request.getEmail());
        i.setCelular(request.getCelular());
        i.setFecha_nacimiento(request.getFecha_nacimiento());
        i.setCodigopostal(request.getCodigopostal());
        i.setDepartamento(request.getDepartamento());
        i.setProvincia(request.getProvincia());
        i.setDistrito(request.getDistrito());
        response.getLista().add(i);

        getJdbcTemplate().update(agregarClienteSQL,
                            request.getNro_documento(),
                            request.getUsuario(),
                            request.getClave(),
                            request.getNombre(),
                            request.getApe_paterno(),
                            request.getApe_materno(),
                            request.getFecha_registro(),
                            request.getTipo_doc(),
                            request.getDireccion(),
                            request.getEmail(),
                            request.getCelular(),
                            request.getFecha_nacimiento(),
                            request.getCodigopostal(),
                            request.getDepartamento(),
                            request.getProvincia(),
                            request.getDistrito()
        );

        return response;
    }
}
