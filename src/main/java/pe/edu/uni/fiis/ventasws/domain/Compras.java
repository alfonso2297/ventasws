package pe.edu.uni.fiis.ventasws.domain;

public class Compras {
    private Integer id_compras;
    private Integer id_proveedor;
    private Integer c_pollo;
    private Integer c_pavita;
    private Integer c_huevos;
    private Integer igv;

    public Integer getIgv() {
        return igv;
    }

    public void setIgv(Integer igv) {
        this.igv = igv;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    private Double precio;

    public Integer getId_compras() {
        return id_compras;
    }

    public void setId_compras(Integer id_compras) {
        this.id_compras = id_compras;
    }

    public Integer getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Integer id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public Integer getC_pollo() {
        return c_pollo;
    }

    public void setC_pollo(Integer c_pollo) {
        this.c_pollo = c_pollo;
    }

    public Integer getC_pavita() {
        return c_pavita;
    }

    public void setC_pavita(Integer c_pavita) {
        this.c_pavita = c_pavita;
    }

    public Integer getC_huevos() {
        return c_huevos;
    }

    public void setC_huevos(Integer c_huevos) {
        this.c_huevos = c_huevos;
    }
}
