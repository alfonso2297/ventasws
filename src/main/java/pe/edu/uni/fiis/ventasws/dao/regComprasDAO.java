package pe.edu.uni.fiis.ventasws.dao;

import pe.edu.uni.fiis.ventasws.domain.rest.regComprasRequest;
import pe.edu.uni.fiis.ventasws.domain.rest.regComprasResponse;

public interface regComprasDAO {

    regComprasResponse registrarCompra (regComprasRequest request);

}
